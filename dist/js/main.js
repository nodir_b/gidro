$(document).ready(function() {

    $('.select__2').select2();

    $("#datepicker").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true
    });

    $('#education-range input').each(function() {
        $(this).datepicker({
            autoclose: true,
            format: " yyyy",
            viewMode: "years",
            minViewMode: "years"
        });
        $(this).datepicker('clearDates');
    });


    $(document).on('click', '#toggle__lock', function() {
        var pwdType = $('.passwordToggle input').attr("type");
        var newType = (pwdType === "password") ? "text" : "password";
        $('.passwordToggle input').attr("type", newType);
    })

    $(document).on('click', '#toggle__sidebar', function() {
        $('body').toggleClass('open__side')
    })

    $(document).on('click', '.sidebar ul li span.first', function() {
        $('body').removeClass('open__side')
    })

    $(document).on('click', '.open__filter', function() {
        $('body').addClass('filter__active')
    })

    $(document).on('click', '#filter__toggle', function() {
        $('body').removeClass('filter__active')
    })

    $(document).on('click', '.sidebar span.first', function() {
        $(this).parent().siblings().find('.inner').slideUp()
        $(this).parent().siblings().removeClass('opened')
        $(this).next().slideToggle()
        $(this).parent().toggleClass('opened')
    })

    $(document).on('click', '.filter span.first', function() {
        $(this).parent().siblings().find('.inner').slideUp()
        $(this).parent().siblings().removeClass('opened')
        $(this).next().slideToggle()
        $(this).parent().toggleClass('opened')
    })

    $(document).on('click', '.graph span.btn', function() {
        $(this).parent().toggleClass('open')
    })

    $(document).on('click', '.graph .bottom button', function() {
        $('.graph ').removeClass('open')
    })
    

    if ($(window).width() < 1210) {
        $('body ').addClass('open__side')
    }



});